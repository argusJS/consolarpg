﻿using System;
using System.Collections.Generic;
using Stats;

namespace Proyectos
{
    class Program
    {
        static void Main(string[] args)
        {
          var profesiones = new Profesiones();
          Profesion miProfesion;
          
          Console.WriteLine("Escoge tu profesión: 1 Guerrero  2 Arquero");

          var input = Console.ReadLine();
          if (input == "1") miProfesion = profesiones.guerrero;
          else miProfesion = profesiones.arquero;

          var presentacion = profesiones.presentaciones[miProfesion.ID];
          Console.WriteLine(presentacion);

          miProfesion.Stats();

            Console.WriteLine("Subir de lvl?  1  Si    2 No");
            var input2 = Console.ReadLine();
          if (input2 == "1") miProfesion.SubirDeLvl();

          miProfesion.Stats();
        

        }
    }
}
