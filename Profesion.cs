using System;
using System.Collections.Generic;

namespace Stats
{

    public class Profesion
    {

        public string Nombre { get; set; }
        public int Daño { get; set; }
        public float Vida { get; set; }

        public int ID { get; set; }


        public Profesion(int ID, string nombre, int daño, float vida)
        {
            this.ID = ID;
            this.Nombre = nombre;
            this.Daño = daño;
            this.Vida = vida;
        }

        public void Presentarse()
        {
            Console.WriteLine($"Hola soy un {this.Nombre}");
        }

        public void SubirDeLvl()
        {
            this.Vida += 50f;
            this.Daño += 10;
        }

        public void Stats()
        {
            Console.WriteLine($"Mis stat son: Vida = {this.Vida}  ...   Daño = {this.Daño}");
        }
    }


    public  class Profesiones
    {
                public Profesion guerrero = new Profesion(0, "Guerrero", 10, 100);
                public Profesion arquero = new Profesion(1, "Arquero", 40, 70);

                public List<string> presentaciones = new List<string>() 
                { 
                    "Los guerreros somos los mejores", 
                    "Los arqueros tenemos la mejor visión"
                };

    }
}